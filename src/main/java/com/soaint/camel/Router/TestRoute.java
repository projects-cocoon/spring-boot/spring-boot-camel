package com.soaint.camel.Router;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Value;

@Component
public class TestRoute extends RouteBuilder {
    @Value("${server.port}")
    String serverPort;

    @Override
    public void configure() throws Exception {
        // All route configuretaion goes here        

        // Example of spring boot camel api doc configuration
        restConfiguration()
            .contextPath("")
            .port(serverPort)
            .enableCORS(true)
            .apiContextPath("/api-doc")
            .apiProperty("api.title", "Test Rest")
            .apiProperty("api.version", "v1")
            .component("servlet")
            .bindingMode(RestBindingMode.json)
            .dataFormatProperty("prettyPrint", "true");

        // Example of Api Call /camel/say/hello
        // The direct: prefix is use to determinate a from call
        rest("/say")
            .get("/hello")
            .to("direct:hello");
        
        // Example of direct method call from rest.to()
        from("direct:hello")
        .transform().constant("Hello World");
    }
    
}
